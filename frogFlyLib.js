function Frog() {
    frog = new Sprite(scene, "frog.png", 250, 150);
    frog.maxSpeed = 10;
    frog.minSpeed = 0;
    frog.setSpeed(0);
    frog.setPosition(300,300)
    frog.setAngle(0)

    frog.controlFrog = function () {
        if (keysDown [K_LEFT])
            {
                this.changeAngleBy(-5);
            }
            
            if (keysDown [K_RIGHT]){
                this.changeAngleBy(5);
            }

            if (keysDown [K_UP]){
                this.changeSpeedBy(1);
                if (this.speed > this.maxSpeed){
                    this.setSpeed(this.maxSpeed);
                }
            }

            if (keysDown [K_DOWN]){
                this.changeSpeedBy(-1);
                if (this.speed  < this.minSpeed){
                    this.setSpeed(this.minSpeed);
                }
            }
    }
    return frog;
}

function Fly() {
    fly = new Sprite(scene, "fly.png", 20, 20);
    fly.setSpeed(20);

    fly.wriggle = function() {
        newDir = (Math.random()*90)-45;
        this.changeAngleBy(newDir);
    }
     return fly;
}